---
title: "Workforce"
---

## Efficiency Formulas

In every case, any value in brackets (e.g. **[DW]**) is the percentage fulfilled to your workforce.  In most cases, this value is simply `1.0`.

### Pioneer
`Pioneer Efficiency = 0.02 * (1+([DW]*10/3)) * (1+([RAT]*4)) * (1+([OVE]*5/6)) * (1+([PWO]*1/11)) * (1+([COF]*2/13))`

### Settler
`Settler Efficiency = 0.01 * (1+([DW]*10/3)) * (1+([RAT]*4)) * (1+([EXO])) * (1+([PT]*5/6)) * (1+([REP]*1/11)) * (1+([KOM]*2/13))`

### Technician
`Technician Efficiency = 0.005 * (1+([DW]*10/3)) * (1+([RAT]*4)) * (1+([MED]*5/6)) * (1+[HMS]) * (1+[SCN]) * (1+([ALE]*2/13)) * (1+([SC]/11))`

### Engineer
`Engineer Efficiency = 0.005 * (1+([DW]*10/3)) * (1+([FIM]*4)) * (1+([MED]*5/6)) * (1+[HSS]) * (1+[PDA]) * (1+([GIN]*2/13)) * (1+([VG]/11))`

### Scientist
**Note: This has not been fully verified**

`Scientist Efficiency = 0.005 * (1+([DW]*10/3)) * (1+([MEA]*4)) * (1+([MED]*5/6)) * (1+[LC]) * (1+[WS]) * (1+([WIN]*2/13)) * (1+([NST]/11))`

## Consumption

Current findings:
- Whenever the POPR (population report) hits for the planet, the new tick time is then and there.
- Whenever you change worker amounts, your new tick time is then and there. Things that affect this:
   - Building a production building where said building would add workers (unknown if it triggers if you add a building w/ no worker count change)
   - Destroying a production building which would reduce the amount of required workers
- Adding capacity triggers the consumption event
- Repairing does not trigger the consumption event

Whenever the above happens, you will immediately have a consumption event and your new consumption time is then.  If, for example:

1. Your last consumption event was at noon
2. You build a new production building at 6pm

Your workers will immediately consume 25% (6 hours = 1/4 of a day) of their daily needs and the new consumption time will be 6pm.

Tracking consumption can be done using the FIOExtension (if APEX is running when the consumption event happens).